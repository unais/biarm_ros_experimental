GETTING STARTED
-------------------
The ROS packages associated with Biarm are:

biarm_moveit_config : the ROS moveit package generated for Biarm

biarm_controller : the package for interfacing with the biarm hardware

biarm_description    : contains the URDF model of the biarm.

INSTALLATION
------------

clone the repo into your ROS catkin workspace
make : using catkin_make


BRINGING UP BIARM IN RVIZ & CONTROLLING THE HARDWARE
-------------------------


BRING UP BIARM USING THE FOLLOWING COMMAND:

	roslaunch biarm_moveit_config  demo.launch 

NOW LAUNCH THE NODES FOR CONTROLLING THE HARDWARE USING THE FOLLOWING COMMAND:
        
	roslaunch biarm_controller biarm_hardware.launch

