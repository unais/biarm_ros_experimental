#!/usr/bin/env python

__version__ = "0.1"
__author__ = "unais@asimovrobotics.com"

'''
This is a python script to start parallel servos of each arm

'''


import rospy
import roslib
roslib.load_manifest('dynamixel_controllers')
from dynamixel_msgs.msg import JointState
from std_msgs.msg import Float64

def arm1_callback(data):
    #print "callback1"
    #print data.current_pos
    curr_pos = data.current_pos
    parallel_servo = rospy.Publisher('/joint15_controller/command',Float64)
    parallel_servo.publish(curr_pos)

def arm2_callback(data):
    #print "callback2"
    #print data.current_pos
    curr_pos = data.current_pos
    parallel_servo = rospy.Publisher('/joint16_controller/command',Float64)
    parallel_servo.publish(curr_pos)

def listener():
    rospy.init_node('parellel_servo_starter', anonymous=True)
    print "parellel_servo_starter node started"
    rospy.Subscriber("/joint2_controller/state",JointState, arm1_callback)
    rospy.Subscriber("/joint9_controller/state",JointState, arm2_callback)
    rospy.spin()


if __name__ == '__main__':
    listener()
