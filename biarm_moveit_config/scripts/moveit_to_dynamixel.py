#!/usr/bin/env python
import rospy
import roslib
import time
#roslib.load_manifest('dynamixel_controllers')
#from dynamixel_msgs.msg import JointState

from moveit_msgs.msg import DisplayTrajectory
from std_msgs.msg import Float64


pub_joint1 = rospy.Publisher('/joint1_controller/command',Float64)
pub_joint2 = rospy.Publisher('/joint2_controller/command',Float64)
pub_joint3 = rospy.Publisher('/joint3_controller/command',Float64)
pub_joint4 = rospy.Publisher('/joint4_controller/command',Float64)
pub_joint5 = rospy.Publisher('/joint5_controller/command',Float64)
pub_joint6 = rospy.Publisher('/joint6_controller/command',Float64)
pub_joint7 = rospy.Publisher('/joint7_controller/command',Float64)
pub_joint8 = rospy.Publisher('/joint8_controller/command',Float64)
pub_joint9 = rospy.Publisher('/joint9_controller/command',Float64)
pub_joint10 = rospy.Publisher('/joint10_controller/command',Float64)
pub_joint11 = rospy.Publisher('/joint11_controller/command',Float64)
pub_joint12 = rospy.Publisher('/joint12_controller/command',Float64)
pub_joint13 = rospy.Publisher('/joint13_controller/command',Float64)
pub_joint14 = rospy.Publisher('/joint14_controller/command',Float64)


def callback(data):
    print "callback"
    way_points = data.trajectory[0].joint_trajectory.points
    joint_names = data.trajectory[0].joint_trajectory.joint_names
    print joint_names
    no_of_joints = len[joint_names]
    trajectory_length = len(way_points)
    #time.sleep(5)
    #print way_points[length - 1]
    #print type(way_points) 
    for i in range(0, trajectory_length-1):
        joint_positions = way_points[i].positions
        print joint_positions
        for j in range(0,no_of_joints-1):
           pub = eval("pub_"+joint_names[j])
           pub.publish(joint_positions[j])
        time.sleep(.05)

def listener():
    rospy.init_node('moveit_to_dynamixel_interface', anonymous=True)
    print "moveit_to_dynamixel_interface node started"
    rospy.Subscriber("/move_group/display_planned_path",DisplayTrajectory, callback)
    rospy.spin()


if __name__ == '__main__':
    listener()
